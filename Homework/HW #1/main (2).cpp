/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 02/24/14
 * HW: #1
 * Problem: 6
 * I certify this is my own work and code
 */

#include < iostream> // won't run with space

using namespace std;

 man() // won't with out type identifier or main spelled incorrectly
{
    int number_of_pods, peas_per_pod, total_peas;
    
    cout < "Please return after entering a number \n"; //won't run without two "<<"
    cout << "Enter the number of pods: \n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod: ";
    cin >> peas_per_pod;
    
    total_peas = number_of_pods * peas_per_pod;
    
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods \n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then \n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods \n";
    
    return 0;
    
    
}
