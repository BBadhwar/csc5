/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 02/24/14
 * HW: #1
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{
    int num1, num2, sum, product;
    
    cout << "Please enter a number: ";
    cin >> num1;
    
    cout << "Now enter a second number: ";
    cin >> num2;
    
    sum = num1 + num2;
    
    product = num1 * num2;
    
    cout << "The sum of these two numbers is: " << sum << endl;
    
    cout << "The product of these two numbers is: " << product;
    
    return 0;
}