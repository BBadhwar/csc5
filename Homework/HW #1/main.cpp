/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 02/24/14
 * HW: #1
 * Problem: 7
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{

    
    cout << "**************************************************** \n \n";
    
    cout << " \t C C C" << "\t \t \t \t" << "S S S S " << endl;
    
    cout << "   C \t" << "   C" << "\t \t  S         S" << endl;
    
    cout << "  C" << " \t \t \t     S" << endl;
    
    cout << "  C" << " \t \t \t      S" << endl;
    
    cout << "  C" << " \t \t \t        S" << endl;
    
    cout << "  C" << " \t \t \t            S" << endl;
    
    cout << "   C" << " \t \t \t               S" << endl;
    
    cout << "    C" << " \t \t \t              S" << endl;
    
    cout << "      C" << " \t \t \t         S" << endl;
    
    cout << "        C C C" << " \t \t  S S S S \n \n" << endl;
    
    cout << "**************************************************** \n \n";
    
    return 0;
}

