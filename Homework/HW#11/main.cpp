/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 06/01/14
 * HW: 11
 * Problem: 1
 * I certify this is my own work and code
 */

#include <iostream>
#include <fstream>
using namespace std;

struct TimeStruct // strucutre
{
    double seconds;
    int minutes;
    int hours;
};

class TimeClass 
{
    private:
        int seconds;
        int minutes;
        int hours;
    public:
        TimeClass(){ //default constructor
            seconds = 0;
            minutes = 0;
            hours = 0;
        }
        TimeClass(int sec, int min, int hrs){ //constructor
            seconds = sec;
            minutes = min;
            hours = hrs;
        }
    
        void setSeconds(int sec){
            seconds = sec;
        }
    
        void setMinutes(int min){
            minutes = min;
        }
    
        void setHours(int hrs){
            hours = hrs;
        }
    
        int getSeconds(){
            return seconds;
        }
    
        int getMinutes(){
            return minutes;
        }
    
        int getHours(){
            return hours;
        }
        void output(ofstream& outfile){
            outfile.open("data.dat");
            
            cout << seconds << endl;
            cout << minutes << endl;
            cout << hours << endl;
            
            outfile << seconds << endl;
            outfile << minutes << endl;
            outfile << hours << endl;
            
            outfile.close();
        }
};

class Date : public TimeClass //Date class inherited from TimeClass
{
    private:
        int months;
        int years;
    public:
        Date(){
            TimeClass();
            months = 0;
            years = 0;
    }
        Date(int month, int year){
            years = year;
            months = month;
    }
        Date(int month, int year, int hrs, int sec, int min){
            
            TimeClass(sec, min, hrs);
            years = year;
            months = month;
    }
    int getMonths(){
        return months;
    }
    
    int getYears(){
        return years;
    }
    void output(ofstream& outfile){
        outfile.open("data.dat");
        
        outfile << months << endl;
        outfile << years << endl;
        
        outfile.close();
    }
};

int main()
{
    TimeClass times;
    TimeStruct structTimes;
    int sec, min, hrs; //seconds, minutes, hours
    int month, year; //month, years
    ofstream outfile;
    
    /*
     
     Strucutre Output:
     
     cout << "Seconds: " <<  times.getSeconds() << endl;
     cout << "Minutes: " << times.getMinutes() << endl;
     cout << "Hours: " << times.getHours() << endl;
     
     */
    
    /*
        when private, main cannot access the class's member
        variables directly and will not compile.
        this will not compile:
    
    cout << "Enter a time in seconds: ";
    cin >> times.seconds;
    cout << "Minutes: ";
    cin >> times.minutes;
    cout << "Hours: ";
    cin >> times.hours;
     
    */
    
    //
    // Using setters here:
    //
    
    cout << "Enter a time in seconds: ";
    cin >> sec;
    times.setSeconds(sec);
    
    cout << "Enter a time in minutes: ";
    cin >> min;
    times.setMinutes(min);
    
    cout << "Enter a time in hours: ";
    cin >> hrs;
    times.setHours(hrs);
    
    times.output(outfile);
    cout << endl;
    
    //
    // Constructor vs Default Constructor
    //
    
    TimeClass defaultconstruct;
    TimeClass construct(sec, min, hrs);
    
    cout << "Default Constructor: " << endl;
    cout << defaultconstruct.getSeconds() << endl;
    cout << defaultconstruct.getMinutes() << endl;
    cout << defaultconstruct.getHours() << endl << endl;
    
    cout << "Constructor: " << endl;
    cout << construct.getSeconds() << endl;
    cout << construct.getMinutes() << endl;
    cout << construct.getHours() << endl << endl;
    
    /*
     
        From Inherited Class:
     
    */
    
    month = 12;
    year = 1;
    
    ofstream outfile2;
    
    Date object1; //outputs default constructor
    Date object2(month, year);
    Date object3(month, year, hrs, sec, min);
    
    cout << "Default Constructor: " << endl;
    cout << object1.getSeconds() << endl;
    cout << object1.getMinutes() << endl;
    cout << object1.getHours() << endl;
    cout << object1.getMonths() << endl;
    cout << object1.getYears() << endl << endl;
    
    cout << "Constructor 1 : " << endl;
    cout << object2.getSeconds() << endl;
    cout << object2.getMinutes() << endl;
    cout << object2.getHours() << endl;
    cout << object2.getMonths() << endl;
    cout << object2.getYears() << endl << endl;
    
    cout << "Constructor 2 : " << endl;
    cout << object3.getSeconds() << endl;
    cout << object3.getMinutes() << endl;
    cout << object3.getHours() << endl;
    cout << object3.getMonths() << endl;
    cout << object3.getYears() << endl << endl;

    return 0;
}

