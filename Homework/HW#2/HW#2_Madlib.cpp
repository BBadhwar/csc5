/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/04/14
 * HW: 2
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    char name1[10], name2[10], food[10], adj[10], color[10], animal[10];
    int num;
    
    cout << "Enter a name: " << endl;
    cin >> name1;
    
    cout << "Enter another name: " << endl;
    cin >> name2;
    
    cout << "Enter a food: " << endl;
    cin >> food;
    
    cout << "Enter a number from 100 - 120: " << endl;
    cin >> num;
    
    if(num < 100 || num > 120)
    {
        cout << "Sorry, you must enter a number from 100 - 120";
    }
    else
    {
    
    cout << "Enter an adjective: " << endl;
    cin >> adj;
    
    cout << "Enter a color:" << endl;
    cin >> color;
    
    cout << "Enter an animal: " << endl;
    cin >> animal;
    cout << endl;
    
    cout << "Dear " << name1 << "," << endl;
    cout << endl;
    
    cout << "I am sorry that I am unable to turn in my homework at this time."
         << " First, I ate a rotten " << food << "," << " which made me turn "
         << color << " and extremely ill. I came down with a fever of " << num
         << " degrees. Next, my " << adj << " pet " << animal
         << " must have smelled the remains of the " << food << " on my homework"
         << " because he ate it. I am currently rewriting my homework and hope "
         << "you will accept it late."
         << endl << endl;
        
    cout << "Sincerely," << endl << name2;
        
    }
    
    return 0;
}

