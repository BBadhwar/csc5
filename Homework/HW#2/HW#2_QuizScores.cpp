/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/04/14
 * HW: 2
 * Problem: 3
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

/*
 *
 */
int main() {
    
    string name1, name2, name3; //student names
    double quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7, quiz8,
    quiz9, quiz10, quiz11, quiz12; // each quiz score
    double avg1, avg2, avg3, avg4; //averages
    
    cout << "Enter the names of three students: "; //get each name
    cin >> name1 >> name2 >> name3;
    cout << endl;
    
    cout << name1 << ", enter your four quiz scores out of 10: "; //gets first set of quizzes
    cin >> quiz1 >> quiz2 >> quiz3 >> quiz4;
    cout << endl;
    
    cout << name2 << ", enter four quiz scores: "; //second set of quizzes
    cin >> quiz5 >> quiz6 >> quiz7 >> quiz8;
    cout << endl;
    
    cout << name3 << ", enter four quiz scores: "; //third set of quizzes
    cin >> quiz9 >> quiz10 >> quiz11 >> quiz12;
    cout << endl;
    
    cout  << "Name" << setw(10) << "Quiz 1" << setw(10) << "Quiz 2" << setw(10)
    << "Quiz 3" << setw(10) << "Quiz 4" << endl;
    cout << "----" << setw(10) << "------" << setw(10) << "------" << setw(10)
    << "------" << setw(10) << "------" << endl; //start of table
    
    cout << left << setw(10) << name1 << setw(10) << quiz1 << setw(10) << quiz2 << setw(10)
    << quiz3 << setw(10) << quiz4 << endl; //row 1
    
    cout << left << setw(10) << name2 << setw(10) << quiz5 << setw(10) << quiz6 << setw(10)
    << quiz7 << setw(10) << quiz8 << endl; //row 2
    
    cout << left << setw(10) << name3 << setw(10) << quiz9 << setw(10) << quiz10 << setw(10)
    << quiz11 << setw(10) << quiz12 << endl; //row 3
    
    cout << endl;
    
    avg1 = (quiz1 + quiz5 + quiz9) / 3; // calculating averages
    avg2 = (quiz2 + quiz6 + quiz10) / 3;
    avg3 = (quiz3 + quiz7 + quiz11) / 3;
    avg4 = (quiz4 + quiz8 + quiz12) / 3;
    
    cout << setprecision(2) << fixed << left << setw(10) << "Average" << setw(10) << avg1 << setw(10)
    << avg2 << setw(10) << avg3 << setw(10) << avg4 << endl; //displays averages
    
    return 0;
}