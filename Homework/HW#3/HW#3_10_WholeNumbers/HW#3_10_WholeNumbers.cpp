/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/14/14
 * HW: 3
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{
    int num, positive_sum = 0, negitive_sum = 0,
    sum_all = 0;
    
    for(int i = 1; i <= 10; i++)
    {
        cout << "Enter a number: ";
        cin >> num;
        
        if(num > 0)
            
            positive_sum = positive_sum + num;
        
        else if(num < 0)
            
            negitive_sum = negitive_sum + num;
        
        else
            
            sum_all = sum_all + num;
    }
    
    cout << endl;
    
    cout << "The sum of all positive numbers is: ";
    cout << positive_sum << endl;
    cout << "The sum of all negitive number is:  ";
    cout << negitive_sum << endl;
    cout << "The sum of all numbers is: ";
    cout << sum_all << endl;
    
    return 0;
}
