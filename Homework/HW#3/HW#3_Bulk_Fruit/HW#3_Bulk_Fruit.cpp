/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/12/14
 * HW: 3 
 * Problem: 2
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{
    int apples, oranges, pears;
    
    cout << "Enter number of apples: "; //gets input for each fruit
    cin >> apples;
    
    cout << "Enter number of oranges: ";
    cin >> oranges;
    
    cout << "Enter number of pears: ";
    cin >> pears;
    cout << endl;
    
    if(apples < oranges && apples < pears) // if apples are the least
    {                                       
        oranges = oranges - apples;
        pears = pears - apples;
        apples = 0; // left amount is always 0 for the least type of fruit
    }
    else if(oranges < apples && oranges < pears) // if oranges are the least
    {
        apples = apples - oranges;
        pears = pears - oranges;
        oranges = 0;
    }
    else // if pears are the least
    {
        apples = apples - pears;
        oranges = oranges - pears;
        pears = 0;
    }
    
    cout << "Number of apples you should leave: " << apples << endl
         << "Number of oranges you should leave: " << oranges << endl
         << "Number of pears you should leave: " << pears << endl;
    
    return 0;
}

