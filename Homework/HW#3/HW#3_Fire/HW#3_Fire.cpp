/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/11/14
 * HW: 3
 * Problem: 3
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{
    int number_of_people, max_cap, must_exclude, may_attend;
    
    cout << "Enter the number of people attending the meeting: ";
    cin >> number_of_people;
    cout << endl;
    
    cout << "Enter the maximum capacity for the room: ";
    cin >> max_cap;
    cout << endl;
    
    if( number_of_people > max_cap)
    {
        must_exclude = number_of_people - max_cap;

        cout << "By law, this meeting cannot be held!" << endl;
        cout << "You must exclude " << must_exclude;
        cout << " people to legally hold this meeting.";
        
    }
    else 
    {
        may_attend = max_cap - number_of_people;
        
        cout << "You may legally hold this meeting." << endl;
        cout << "The law allows " << may_attend;
        cout << " more people to still attend this meeting.";
    }

    return 0;
}

