/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 3
 * Problem: 1
 * I certify this is my own work and code
 */

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string num; //input from user
    int dash_pos; //position of dash within string
    
    cout << "Please enter a phone number with a dash anywhere in between: ";
    cin >> num;
    
    dash_pos = num.find("-"); //gets position
    
    // this seperates the string into to two strings
    // one string before dash and one after dash
    
    cout << num.substr(0,dash_pos) << num.substr(dash_pos + 1);
    
    return 0;
    
}
