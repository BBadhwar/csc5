/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/21/14
 * HW: 4
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int weight, height, age;
    char gender;
    double bmr, num_of_bars;
    
    do
    {
    
    cout << "Enter your weight in pounds: ";
    cin >> weight;
    
    cout << "Enter your height in inches: ";
    cin >> height;
    
    cout << "Enter your age: ";
    cin >> age;
    
    cout << "Enter \"M\" for male or \"F\" for female : ";
    cin >> gender;
    
    if(gender == 'F')
    {
        bmr = 655 + (4.3 * weight) + (4.7 * height) -
        (4.7 * age);
    }
    else
    {
        bmr = 66 + (6.3 * weight) + (12.9 * height) -
        (6.8 * age);
    }
    
    cout << endl;
    
    num_of_bars = bmr / 230;
    
    cout << "Your BMR is: " << bmr << endl;
    cout << "You could eat " << setprecision(2) << num_of_bars;
    cout << " chocolate bars to maintain your weight.";
    cout << endl << endl;
    
    } while(bmr > 0);
    
    return 0;
}

