/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/21/14
 * HW: 4
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

/*
 *
 */
int main() {
    
    int num_of_inputs, total_points = 0, total_possible = 0,
    points_possible, score, percentage;
    
    cout << "How many exercises to input? ";
    cin >> num_of_inputs;
    cout << endl;
    
    for( int i = 0; i < num_of_inputs; i++)
    {
        
        cout << "Score received for exercise: ";
        cin >> score;
        
        cout << "Total points possible for exercise: ";
        cin >> points_possible;
        cout << endl;
        
        total_points = total_points + score;
        
        total_possible = total_possible + points_possible;
    }
    
    cout << "Your total score is: " << total_points << " out of "
    << total_possible << endl;
    
    percentage = static_cast<double>(total_points) / (total_possible) * 100;
    
    cout << "or " << fixed << setprecision(2) << percentage << " % ";
    
    return 0;
}

