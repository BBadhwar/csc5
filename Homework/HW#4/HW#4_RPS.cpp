/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/21/14
 * HW: 4
 * Problem: 6 & 7
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

int main()
{
    char player_one, player_two;
    
    cout << "Choose \"P\" for paper, \"S\" for scissors or \"R\" for rock.";
    cout << endl << endl;
    
    do
    {
       
    cout << "Player 1: ";
    cin >> player_one;
    
    
    cout << "Player 2: ";
    cin >> player_two;
    cout << endl;
        
    
    if((player_one == 'R' || player_two == 'R'))
    {
       if ((player_one == 'R' && player_two == 'R'))
       {
           cout << "Its a tie!" << endl;
       }
       else if((player_one == 'S' || player_two == 'S'))
       {
           cout << "Rock breaks scissors. ";
           
           if(player_one == 'R')
           {
               cout << "Player 1 wins!" << endl;
           }
           else
           {
               cout << "Player 2 wins!" << endl;
           }
       }
       else if((player_one == 'P' || player_two == 'P'))
       {
           cout << "Paper covers rock. ";
           
           if(player_one == 'R')
                         {
               cout << "Player 2 wins!" << endl;
           }
           else
           {
               cout << "Player 1 wins!" << endl;
           }
       }
    }
    else if((player_one == 'S' || player_two == 'S'))
    {
        if((player_one == 'S' && player_two == 'S'))
        {
            cout << "It's a tie!" << endl;
        }
        else if((player_one == 'P' || player_two == 'P'))
        {
            cout << "Scissors cuts paper. ";
            
            if(player_one == 'S')
            {
                cout << "Player 1 wins!" << endl;
            }
            else
            {
                cout << "Player 2 wins!" << endl;
        
            }
        }
    }
    else if((player_one == 'P' || player_two == 'P'))
    {
        if(player_one == 'P' && player_two == 'P')
        {
            cout << "It's a tie!" << endl;
        }
    }
    
    cout << endl;
        
    } while(player_one == 'R' || 'P' || 'S');
    
    
    return 0;
}

