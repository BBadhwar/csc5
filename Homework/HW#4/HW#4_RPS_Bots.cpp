/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/21/14
 * HW: 4
 * Problem: 7 & 8
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int player_one_num, player_two_num, user_choice, cont;
    char player_one, player_two;
    
    cout << "Choose \"P\" for paper, \"S\" for scissors or \"R\" for rock.";
    cout << endl << endl;
    
    cout << "Enter '0' to play with a friend";
    cout << " or '1' to watch two computers play: ";
    
    cin >> user_choice;
    cout << endl;
        
     srand(time(0));
    
    do
    {
        /* This dictates whether the user wants to play
         * with friend or wants to watch as two 
         * computers play
         */
        
        if(user_choice == 1)
        {
        
            player_one_num = rand() % 3;
            player_two_num = rand() % 3;
        
            if (player_one_num == 0 || player_two_num == 0)
            {
                if (player_one_num == 0)
                {
                    player_one = 'R';
                }
                else if (player_two_num == 0)
                {
                    player_two = 'R';
                }
                else if(player_one == 0 && player_two == 0)
                {
                    (player_one = 'R') && (player_two = 'R');
                }
            }
            else if (player_one_num == 1 || player_two_num == 1)
            {
                if (player_one_num == 1)
                {
                    player_one = 'P';
                }
                else if (player_two_num == 1)
                {
                    player_two = 'P';
                }
                else
                {
                    (player_one = 'P') && (player_two = 'P');
                }
            }
            else if (player_one_num == 2 || player_two_num == 2)
            {
                if (player_one_num == 2)
                {
                    player_one = 'S';
                }
                else if (player_two_num == 2)
                {
                    player_two = 'S';
                }
                else if(player_one == 2 && player_two == 2)
                {
                    (player_one = 'S') && (player_two = 'S');
                }
            }
            
            
            
            cout << "Player 1: " << player_one << endl;
            cout << "Player 2: " << player_two << endl;
            
        }
        else if (user_choice == 0)
        {
            cout << "Player 1: ";
            cin >> player_one;
            
            cout << "Player 2: ";
            cin >> player_two;
        }
        
        
        /* Once the user decides whether they want to play or have
         * computers play, each decision is made by the following block 
         * of code
         */
            
        
        if((player_one == 'R' || player_two == 'R'))
        {
            if ((player_one == 'R' && player_two == 'R'))
            {
                cout << "Its a tie!" << endl;
            }
            else if((player_one == 'S' || player_two == 'S'))
            {
                cout << "Rock breaks scissors. ";
                
                if(player_one == 'R')
                {
                    cout << "Player 1 wins!" << endl;
                }
                else
                {
                    cout << "Player 2 wins!" << endl;
                }
            }
            else if((player_one == 'P' || player_two == 'P'))
            {
                cout << "Paper covers rock. ";
                
                if(player_one == 'R')
                {
                    cout << "Player 2 wins!" << endl;
                }
                else
                {
                    cout << "Player 1 wins!" << endl;
                }
            }
        }
        else if((player_one == 'S' || player_two == 'S'))
        {
            if((player_one == 'S' && player_two == 'S'))
            {
                cout << "It's a tie!" << endl;
            }
            else if((player_one == 'P' || player_two == 'P'))
            {
                cout << "Scissors cuts paper. ";
                
                if(player_one == 'S')
                {
                    cout << "Player 1 wins!" << endl;
                }
                else
                {
                    cout << "Player 2 wins!" << endl;
                    
                }
            }
        }
        else if((player_one == 'P' || player_two == 'P'))
        {
            if(player_one == 'P' && player_two == 'P')
            {
                cout << "It's a tie!" << endl;
            }
        }
        
        cout << endl;
        
        cout << "Enter '1' to play again: ";
        cin >> cont;
        cout << endl;
        
    } while(cont == 1);
    
    
    return 0;
}
    


    
     

