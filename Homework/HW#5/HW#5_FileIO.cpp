/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/28/14
 * HW: 5
 * Problem: 7
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;

const double gallons_per_liter = .264179;

double mpg(double liters_consumed, double miles)
{
    double gallons;
    
    gallons = liters_consumed * gallons_per_liter;
    
    double mpg;
    
    mpg = miles / gallons;
    
    return mpg;
}

int main()
{
    
    double liters_consumed, miles, miles_per_gallon;
    char user;
    fstream data;
    
    data.open("data.dat");
    
    do
    {
        data >> liters_consumed;
        data >> miles;
        
        miles_per_gallon = mpg(liters_consumed, miles);
        
        cout << "Your car has achieved: " << miles_per_gallon << " per gallon.";
        cout << endl << endl;
        cout << "Try again? (Y/N): ";
        cin >> user;
        cout << endl;
        
    } while (user == 'Y' || user == 'y');
    
    data.close();
    
    
    return 0;
}

