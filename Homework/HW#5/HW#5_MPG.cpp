/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/28/14
 * HW: 5
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

const double gallons_per_liter = .264179;

double mpg(double liters_consumed, double miles)
{
    double gallons; 
    
    gallons = liters_consumed * gallons_per_liter;
    
    double mpg;
    
    mpg = miles / gallons;
    
    return mpg;
}

int main()
{
    
    double liters_consumed, miles, miles_per_gallon;
    char user;
    
    do
    {
        cout << "Enter how many liters of gasoline consumed by your car: ";
        cin >> liters_consumed;
        cout << "Enter number of miles traveled by car: ";
        cin >> miles;
        cout << endl;
    
        miles_per_gallon = mpg(liters_consumed, miles);
    
        cout << "Your car has achieved: " << miles_per_gallon << " per gallon.";
        cout << endl << endl;
        cout << "Try again? (Y/N): ";
        cin >> user;
        cout << endl;
        
    } while (user == 'Y' || user == 'y');
    
    
    return 0;
}

