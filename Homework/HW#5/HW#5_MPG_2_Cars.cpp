/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/28/14
 * HW: 5
 * Problem: 9
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;

const double gallons_per_liter = .264179;

double mpg(double liters_consumed, double miles)
{
    double gallons;
    
    gallons = liters_consumed * gallons_per_liter;
    
    double mpg;
    
    mpg = miles / gallons;
    
    return mpg;
}

int main()
{
    
    double liters_consumed_1, miles_1, miles_per_gallon_1;
    double liters_consumed_2, miles_2, miles_per_gallon_2;
    fstream data;
    
    data.open("data.dat");

        data >> liters_consumed_1;
        data >> miles_1;
        data >> liters_consumed_2;
        data >> miles_2;
        
        
        miles_per_gallon_1 = mpg(liters_consumed_1, miles_1);
        miles_per_gallon_2 = mpg(liters_consumed_2, miles_2);
        
        cout << "Car 1 has achieved: " << miles_per_gallon_1 << " per gallon.";
        cout << endl;
        cout << "Car 2 has achieved: " << miles_per_gallon_2 << " per gallon.";
        cout << endl;
    
        if (miles_per_gallon_1 > miles_per_gallon_2)
            cout << "Car 1 is more efficient.";
        else
            cout << "Car 2 is more efficient.";

    data.close();
    
    
    return 0;
}

