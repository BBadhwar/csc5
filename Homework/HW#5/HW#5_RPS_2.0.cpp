/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/21/14
 * HW: 5
 * Problem: 3 & 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

void problem_1()
{
    char player_one, player_two;
    
    cout << "Choose your move: (R) (P) (S)" << endl;
    
    do
    {
        
        cout << " Player 1: ";
        cin >> player_one;
        cout << " Player 2: ";
        cin >> player_two;
        
        switch (player_one)
        {
            case 'R':
            {
                switch (player_two)
                {
                    case 'R':
                        cout << "It's a tie!";
                        break;
                    case 'r':
                        cout << "It's a tie!";
                        break;
                    case 'P':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'p':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    case 's':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            case 'r':
            {
                switch (player_two)
                {
                    case 'R':
                        cout << "It's a tie!";
                        break;
                    case 'r':
                        cout << "It's a tie!";
                        break;
                    case 'P':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'p':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    case 's':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            case 'P':
            {
                switch (player_two)
                {
                    case 'P':
                        cout << "It's a tie!";
                        break;
                    case 'p':
                        cout << "It's a tie!";
                        break;
                    case 'R':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'r':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'S':
                        cout << "Scissors cuts paper. Player 2 wins!";
                        break;
                    case 's':
                        cout << "Scissors cuts paper. Player 2 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            case 'p':
            {
                switch (player_two)
                {
                    case 'P':
                        cout << "It's a tie!";
                        break;
                    case 'p':
                        cout << "It's a tie!";
                        break;
                    case 'R':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'r':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'S':
                        cout << "Rock beats scissors. Player 2 wins!";
                        break;
                    case 's':
                        cout << "Rock beats scissors. Player 2 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            case 'S':
            {
                switch (player_two)
                {
                    case 'P':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'p':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'R':
                        cout << "Rock breaks scissors. Player 2 wins!";
                        break;
                    case 'r':
                        cout << "Rock breas scissors. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "It's a tie!";
                        break;
                    case 's':
                        cout << "Its a tie!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            case 's':
            {
                switch (player_two)
                {
                    case 'P':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'p':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'R':
                        cout << "Rock breaks scissors. Player 2 wins!";
                        break;
                    case 'r':
                        cout << "Rock breas scissors. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "It's a tie!";
                        break;
                    case 's':
                        cout << "Its a tie!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                }
            }
                break;
                
            default:
            {
                cout << "Both players must enter valid choices!";
            }
                break;
                
        }
        
        cout << endl << endl;
        
    } while (1 < 2);
}

void problem_2()
{
    int comp_one_num, comp_two_num;
    char computer_one, computer_two;
    char user = '1';
    
    srand(time(0));
    
    while (user == '1')
        
    {
        
        comp_one_num = rand() % 3;
        comp_two_num = rand() % 3;
        
        switch (comp_one_num)
        {
            case 0:
                computer_one = 'R';
                break;
            case 1:
                computer_one = 'P';
                break;
            case 2:
                computer_one = 'S';
                break;
        }
        switch (comp_two_num)
        {
            case 0:
                computer_two = 'R';
                break;
            case 1:
                computer_two = 'P';
                break;
            case 2:
                computer_two = 'S';
                break;
        }
        
        cout << "Player 1: " << computer_one << endl;
        cout << "Player 2: " << computer_two << endl;
        
        switch (computer_one)
        {
            case 'R':
            {
                switch (computer_two)
                {
                    case 'R':
                        cout << "It's a tie!";
                        break;
                    case 'r':
                        cout << "It's a tie!";
                        break;
                    case 'P':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'p':
                        cout << "Paper covers rock. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    case 's':
                        cout << "Rock beats scissors. Player 1 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                        break;
                }
            }
                
                break;
                
            case 'P':
            {
                switch (computer_two)
                {
                    case 'P':
                        cout << "It's a tie!";
                        break;
                    case 'p':
                        cout << "It's a tie!";
                        break;
                    case 'R':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'r':
                        cout << "Paper covers rock. Player 1 wins!";
                        break;
                    case 'S':
                        cout << "Scissors cuts paper. Player 2 wins!";
                        break;
                    case 's':
                        cout << "Scissors cuts paper. Player 2 wins!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                        break;
                }
            }
                
                break;
                
            case 'S':
            {
                switch (computer_two)
                {
                    case 'P':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'p':
                        cout << "Scissors cuts paper. Player 1 wins!";
                        break;
                    case 'R':
                        cout << "Rock breaks scissors. Player 2 wins!";
                        break;
                    case 'r':
                        cout << "Rock breas scissors. Player 2 wins!";
                        break;
                    case 'S':
                        cout << "It's a tie!";
                        break;
                    case 's':
                        cout << "Its a tie!";
                        break;
                    default:
                        cout << "Both players must enter valid choices!";
                        break;
                }
            }
                
                break;
                
            default:
            {
                cout << "Both players must enter valid choices!";
                break;
            }
        }
        
        cout << endl << endl;
        cout << "Enter (1) to continue: ";
        cin >> user;
        cout << endl;
        
    }
}

int main()
{
    char user;
    
    cout << setw(30) << "*ROCK PAPER SCISSORS*" << endl;
    cout << "Select (1) to play with a friend" << endl;
    cout << "select (2) to watch two computers play" << endl;
    cout << ": ";
    cin >> user;
    cout << endl;
    
    switch (user)
    {
        case '1':
        {
            problem_1();
            break;
        }
        case '2':
        {
            problem_2();
            break;
        }
    }
    
    return 0;
}



