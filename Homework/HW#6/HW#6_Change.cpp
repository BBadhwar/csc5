/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 6
 * Problem: 7
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

void num_of_quarters(int& quarters, int& amount_left, int coin_value);
void num_of_dimes(int& dimes, int& amount_left);
void num_of_pennies(int& pennies, int& amount_left);

int main()
{
    int coin_value, amount_left = 0;
    int quarters = 0, dimes = 0, pennies = 0;
    char user = 'y';
    
    while (user == 'Y' || user == 'y')
    {
        cout << "Enter a coin value from 1 - 99:";
        cin >> coin_value;
    
        num_of_quarters(quarters, amount_left, coin_value);
        num_of_dimes(dimes, amount_left);
        num_of_pennies(pennies, amount_left);
    
        cout << quarters << " Quarter(s)" << endl;
        cout << dimes << " Dime(s)" << endl;
        cout << pennies << " Pennie(s)" << endl;
        cout << "Try again? (Y/N): ";
        cin >> user;
        cout << endl;
    }
    
    return 0;
}

void num_of_quarters(int& quarters, int& amount_left, int coin_value)
{
    quarters = coin_value / 25;
    
    amount_left = coin_value - (quarters * 25);
}
void num_of_dimes(int& dimes, int& amount_left)
{
    dimes = amount_left / 10;
    
    amount_left -= dimes * 10;
    
}
void num_of_pennies(int& pennies, int& amount_left)
{
    pennies = amount_left;
}

