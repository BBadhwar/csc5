/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 6
 * Problem: 6
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

const double METERS_PER_FOOT = 0.3048;

double meters(int feet);
double centimeters(int inches);
void output(int feet, int inches);

int main()
{
    int feet, inches;
    char user = 'y';
    
    while (user == 'Y' || user == 'y')
    {
        
        cout << "Enter an amount in feet: ";
        cin >> feet;
        cout << "Enter a length in inches: ";
        cin >> inches;
        cout << endl;
        
        output(feet, inches);
        
        cout << "Try again? (Y/N): ";
        cin >> user;
        cout << endl;
        
    }
    
    return 0;
}

double meters(int feet)
{
    double meters;
    
    meters = feet * METERS_PER_FOOT;
    
    return meters;
}

double centimeters(int inches)
{
    double centimeters, feet;
    
    feet = inches / 12;
    
    centimeters = (feet * METERS_PER_FOOT) * 100;
    
    return centimeters;
}

void output(int feet, int inches)
{
    cout << feet << " feet is: " << meters(feet) << " meters" << endl;
    cout << inches << " inches is: " << centimeters(inches);
    cout << " centimeters" << endl;
}




