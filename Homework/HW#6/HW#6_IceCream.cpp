/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/30/14
 * HW: 6
 * Problem: 2
 * I certify this is my own work and code
 */

#include <iostream>
#include <iomanip>
using namespace std;

void portion(int customers, double ice_cream_oz); // num of customers and amount of ice cream
                                                // are inputs. Returns portion size for
int main()                                      // each customer. Inputs don't change.
{
    double ice_cream_oz;
    int customers;
    
    cout << "Enter amount of ice cream in ounces: ";
    cin >> ice_cream_oz;
    cout << "Enter how many people are in your group: ";
    cin >> customers;
    
    portion(customers, ice_cream_oz);
    
    return 0;
}

void portion(int customers, double ice_cream_oz)
{
    
    double portion_size = ice_cream_oz / customers;
    
    cout << "Each member in your group should recieve " << portion_size;
    cout << fixed << setprecision(2) << " oz of ice cream.";
}

