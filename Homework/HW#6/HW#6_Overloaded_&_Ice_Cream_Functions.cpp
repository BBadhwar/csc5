/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 6
 * Problem: 3
 * I certify this is my own work and code
 */

#include <iostream>
#include <iomanip>
using namespace std;

void overload_function(); // for overload problem
double max(double x, double y, double z);
double max(double x, double y);

void ice_cream(); // for ice cream problem
void portion(int customers, double ice_cream_oz);

int main()
{
    int user;
    
    cout << "Select 1 for overload function." << endl;
    cout << "Select 2 for ice cream portion function." << endl;
    cin >> user;
    
    switch (user)
    {
        case 1:
            overload_function();
            break;
        case 2:
            ice_cream();
    }

    return 0;
}

void ice_cream() // doesn't return a value and does not use parameters
{
    double ice_cream_oz;
    int customers;
    
    cout << "Enter amount of ice cream in ounces: ";
    cin >> ice_cream_oz;
    cout << "Enter how many people are in your group: ";
    cin >> customers;
    
    portion(customers, ice_cream_oz);
}

void portion(int customers, double ice_cream_oz) // calculates and outputs portion size
{
    
    double portion_size = ice_cream_oz / customers;
    
    cout << "Each member in your group should recieve " << portion_size;
    cout << fixed << setprecision(2) << " oz of ice cream.";
}

void overload_function()
{
    double x = 10, y = 14.5, z = 55.2; // z is largest when 3 parameters are used
    // y is largest when 2 variables are used
    cout << max(x,y,z) << endl;
    cout << max(x,y) << endl;
}

double max(double x, double y, double z) //takes three variables. Non-referenced.
{
    if (x > y && x > z)
        return x;
    
    else if (y > x && y > z)
        return y;
    
    else
        return z;
}

double max(double x, double y) //takes two vatriables. Non-referenced.
{
    if (x > y)
        return x;
    
    else
        return y;
}

