/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/30/14
 * HW: 6
 * Problem: 1
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

double max(double x, double y, double z); //returns highest number. no reference needed
double max(double x, double y);

int main()
{
    double x = 10, y = 14.5, z = 55.2; // z is largest when 3 parameters are used
                                       // y is largest when 2 parameters are used
    cout << max(x,y,z) << endl;
    cout << max(x,y) << endl;
    
    return 0;
}

double max(double x, double y, double z)
{
    if (x > y && x > z)
        return x;
    
    else if (y > x && y > z)
        return y;
    
    else
        return z;
}

double max(double x, double y)
{
    if (x > y)
        return x;
    
    else
        return y;
}

