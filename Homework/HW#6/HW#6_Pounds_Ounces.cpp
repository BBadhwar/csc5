/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 6
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

const double POUNDS_PER_KILOGRAM = 2.2046;

// There are 2.2046 pounds in a kilogram
// 1000 grams in a kilogram, and 16 ounces in a pound.
//

double kilogram(double pounds);
double grams(double ounces);
void output(double pounds, double ounces);

int main()
{
    double pounds, ounces;
    char user = 'Y';
    
    while (user == 'Y' || user == 'y')
    {
    
        cout << "Enter weight in pounds: ";
        cin >> pounds;
        cout << "Enter weight in ounces: ";
        cin >> ounces;
    
        output(pounds, ounces);
        
        cout << endl;
        cout << "Try again? (Y/N): ";
        cin >> user;
        cout << endl;
    }
    
    return 0;
}

double kilogram(double pounds)
{
    double kilogram;
    
    kilogram = pounds / POUNDS_PER_KILOGRAM;
    
    return kilogram;
}

double grams(double ounces)
{
    double grams;
    
    grams =  ((ounces / 16) / POUNDS_PER_KILOGRAM) * 1000;
    
    return grams;
}

void output(double pounds, double ounces)
{
    cout << pounds << " pounds is: " << kilogram(pounds)
         << " kilograms." << endl;
    cout << ounces << " ounces is: " << grams(ounces)
         << " grams." << endl;
}

