/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 6
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
using namespace std;

void swap(int& x, int& y)
{
    int temp = x;
    x = y;
    y = temp;
}

int main()
{
    int x;
    int y;
    
    cout << "Enter a number for x: ";
    cin >> x;
    cout << "Enter a number for y: ";
    cin >> y;
    
    cout << "X is: " << x << endl << "Y is: " << y << endl;
    
    swap(y,x);
    
    cout << "X is: " << x << endl << "Y is: " << y << endl;

    return 0;
}

