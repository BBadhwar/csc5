/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 6
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;

void testFunction(vector<int> v1); //driver to test checkVector
int checkVector(vector<int> v1, int num);
int main()
{
    int loc; 
    int num;
    vector<int> v1;
    
    v1.push_back(5);
    v1.push_back(10);
    v1.push_back(15);
    v1.push_back(20);
    v1.push_back(25);
    
    cout << "Vector: ";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    
    cout << endl;
    cout << "Enter a number you would like to check: ";
    cin >> num;
    
    loc = checkVector(v1, num); //location of vector
    
    if (loc != -1) 
    {
        cout << num << " is found in vector.";
    }
    else //if -1 is returned
        cout << num << " is not found in vector.";
    
    cout << endl << endl;
    
    testFunction(v1);//driver for checkVector
    
    return 0;
}

int checkVector(vector<int> v1, int num)
{
    for (int i = 0; i < v1.size(); i++)
    {
        if (num == v1[i]) //if number exists in vector return its location
        {
            return i;
        }
    }
    
    return -1; //if number does not exist return a value of -1
}

void testFunction(vector<int> v1)
{
    cout << checkVector(v1,5) << endl; //should output location (1)
    cout << checkVector(v1,6) << endl; //should output -1
    cout << checkVector(v1,25) << endl; //should output location (4)
}

