/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 9
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

void delete_repeats(char a[], int size);
int main()
{
    char a[10] = {'a','b','a','e','r','e', 'a'};
    int size = 7;
    
    cout << "Array before: ";
    for (int i = 0; i < size; i++)
        cout << a[i] << " ";
    cout << endl;
    
    delete_repeats(a, size);
    
    return 0;
}

void delete_repeats(char a[], int size)
{
    int loc_a_size = size;
    int repeat_loc[loc_a_size];
    for (int i = 0; i < size; i++)
        repeat_loc[i] = 0;
    
    
    //for loop finds location of repeats
    // and stores them in a parallel array called
    // "repeat_loc"
    
    for (int i = size; i > 0; i--) 
    {
        for (int j = i; j > 0; j--)
        {
            //if a character is repeated, +1 is added to location in a
            //parallel array
            if (a[i] == a[j - 1] && repeat_loc[i] == 0)
            {
                repeat_loc[i] += 1;
                size--; // for each repeat, size is reduced by one
            }
        }
    }
    
    
    cout << "Repeat array (shows location of repeats): ";
    for (int i = 0; i < loc_a_size; i++)
    {
        cout << repeat_loc[i] << " ";
        
    }
     
    cout << endl;
    
    
    //for loop replaces repeated element
    // with the next element in the sequence
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size; j++)
        {
            if (repeat_loc[i] == 1)
            {
                a[j] = a[j + 1];
            }
        }
    }
    
    cout << "Array after: ";
    for (int i = 0; i < size; i++)
       cout << a[i] << " ";

}
