/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 7
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;

void testFunction(vector<int> v1); //driver to test checkVector
int checkVector(vector<int> v1, int num); //checks vector 
int main()
{
    int loc;
    int num;
    vector<int> v1;
    
    while (1 > 0) //always true; for continuous input.
    {
    
        cout << "Vector: ";
        for (int i = 0; i < v1.size(); i++)
        {
            cout << v1[i] << " ";
        }
    
        cout << endl;
        cout << "Enter a number you would like add to the vector: ";
        cin >> num;
    
        loc = checkVector(v1, num); //location of vector
    
        if (loc != -1)
        {
            cout << num << " is already in the vector. It cannot be added.";
        }
        else //if -1 is returned, meaning num doesnt exist in vector
        {
            v1.push_back(num);
            cout << num << " was added to the vector.";
        }
    
        cout << endl << endl;
    
        //testFunction(v1);//driver for checkVector
        
    }
    
    return 0;
}

int checkVector(vector<int> v1, int num)
{
    for (int i = 0; i < v1.size(); i++)
    {
        if (num == v1[i]) //if number exists in vector, return its location
        {
            return i;
        }
    }
    
    return -1; //if number does not exist return a value of -1
}

void testFunction(vector<int> v1)
{
    cout << checkVector(v1,5) << endl; //should output location (1)
    cout << checkVector(v1,6) << endl; //should output -1
    cout << checkVector(v1,25) << endl; //should output location (4)
}

