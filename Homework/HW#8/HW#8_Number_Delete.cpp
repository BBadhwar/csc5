/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 3 & 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;

int checkVector(vector<int> v1, int num); //checks to see if num is valid; returns location
vector<int> removeVector(vector<int> &v1, int num_loc);//returns new vector
int main()
{
    int num;
    int num_loc;
    bool num_exists;
    vector<int> v1;
    
    v1.push_back(3); //fills vector
    v1.push_back(6);
    v1.push_back(9);
    v1.push_back(14);
    v1.push_back(20);
    v1.push_back(25);
    
    cout << "Vector before: "; //outputs vector
    for (int i = 0; i < v1.size();i++)
        cout << v1[i] << " ";
    cout <<endl;
    
    do
    {
        cout << "Enter a number you would like to delete: ";
        cin >> num;
        
        if (checkVector(v1, num) != -1) // number exists in vector
        {
            num_loc = checkVector(v1,num);
            num_exists = true;
        }
        else
        {
            cout << "Error! Number does not exist in vector.";
            cout << endl << endl;
            num_exists = false;
        }
        
    } while (num_exists == false); //repeats until user enters a valid number
    
    v1 = removeVector(v1, num_loc); //removes number and assigns new values to vector
    
    cout << "Vector after: "; //outputs new vector
    for (int i = 0; i < v1.size();i++)
        cout << v1[i] << " ";
    
    return 0;
}

int checkVector(vector<int> v1, int num)
{
    for (int i = 0; i < v1.size(); i++)
    {
        if (num == v1[i]) // if number is in vector, return its location
        {
            return i;
        }
    }
    
    return -1; // if num not found, return -1
}

vector<int> removeVector(vector<int> &v1, int num_loc)
{
    for (int i = 0; i < v1.size(); i++)
    {
        v1[num_loc + i] = v1[num_loc + 1 + i];
    }
    
    v1.pop_back();
    
    return v1;
}


