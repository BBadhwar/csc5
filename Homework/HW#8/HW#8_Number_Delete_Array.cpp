/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;

int checkArray(int v1[], int num, int size); //checks to see if num is valid; returns location
void removeArray(int v1[], int num_loc, int size);//returns new array
int main()
{
    int num;
    int num_loc;
    bool num_exists;
    int v1[6] = {5,10,12,23,30,35};
    int size = 6;
    
    cout << "Array before: "; //outputs vector
    for (int i = 0; i < size;i++)
        cout << v1[i] << " ";
    cout <<endl;
    
    do
    {
        cout << "Enter a number you would like to delete: ";
        cin >> num;
        
        if (checkArray(v1, num, size) != -1) // number exists in array
        {
            num_loc = checkArray(v1,num, size);
            num_exists = true;
        }
        else
        {
            cout << "Error! Number does not exist in vector.";
            cout << endl << endl;
            num_exists = false;
        }
        
    } while (num_exists == false); //repeats until user enters a valid number
    
    removeArray(v1, num_loc, size); //removes number and assigns new values to array
    
    return 0;
}

int checkArray(int v1[], int num, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (num == v1[i]) // if number is in array, return its location
        {
            return i;
        }
    }
    
    return -1; // if num not found, return -1
}

void removeArray(int v1[], int num_loc, int size)
{
    for (int i = 0; i < size; i++)
    {
        v1[num_loc + i] = v1[num_loc + 1 + i];
    }
    
    cout << "Array after: ";
    for (int i = 0; i < size - 1; i++)
    {
        cout << v1[i] << " ";
    }

}

