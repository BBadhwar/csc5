/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 8
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

int main()
{
    srand(time(0));
    
    int num_of_names;
    int id_num;
    string name;
    vector<string> name_vector;
    vector<int> id;
    
    cout << "How many IDs would you like to make: ";
    cin >> num_of_names;
    
    for (int i = 0; i < num_of_names; i++)
    {
        cout << "Enter a name:";
        cin >> name;
        
        name_vector.push_back(name);
        
        id_num = rand() % 40 + 1;
        
        for (int j = 0; j < id.size(); j++)
        {
            while (id_num == id[j])
                id_num = rand() % 40 + 1;
                
        }
        
        id.push_back(id_num);
    }
    
    cout << endl;

    for (int i = 0; i < num_of_names; i++)
    {
        cout << name_vector[i] << ":" << "ID# " << id[i] << endl;
    }
        
    return 0;
}

