/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 8
 * Problem: 1 & 2
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;

void remove_vector (vector<int> &v1, int loc);
void remove_array (int a1[], int loc, int a1_size);
int main()
{
    int a1[5] = {3, 5, 10, 17, 30};
    int a1_size = 5;
    vector<int> v1; // empty vector
    int loc = 3; // 4th element in vector (17)
    
    v1.push_back(3);
    v1.push_back(5);
    v1.push_back(10);
    v1.push_back(17); //this value will be removed
    v1.push_back(30); // fills vector with 5 elements 
    
    cout << "Before (Vector): "; //outputs before vector
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    
    cout << endl;
    
    cout << "Before (Array): ";
    for (int i = 0; i < a1_size; i++)
    {
        cout << a1[i] << " ";
    }
    
    cout << endl;
    
    remove_vector(v1, loc); //function to remove an element from vector
    remove_array(a1, loc, a1_size); //function to remove an element from array
    
    return 0;
}

void remove_vector(vector<int> &v1, int loc)
{

    for (int i = 0; i < v1.size(); i++)
    {
        v1[loc + i] = v1[loc + i + 1];
    }
    
    v1.pop_back();
    
    cout << "After (Vector): ";
    
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    
    cout << endl;
}

void remove_array (int a1[], int loc, int a1_size)
{
    a1[loc] = a1[loc + 1];
    
    cout << "After (Array): ";
    
    for (int i = 0; i < a1_size; i++)
    {
        a1[loc + i] = a1[loc + i + 1];
    }
    
    for (int i = 0; i < a1_size; i++)
    {
        cout << a1[i] << " ";
    }
}

