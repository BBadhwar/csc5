/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/13/14
 * HW: 9
 * Problem: 1
 * I certify this is my own work and code
 */

#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

string getInput(); //string input by user
void loadVectors(vector<int>& count,vector<char>& letters, string& input);
void deleteAndCount(vector<char>& letters, vector<int>& count);
void sort(vector<int>& count, vector<char>& letters);
void output(vector<int>& count, vector<char>& letters);

int main()
{
    
    // "letters" holds each character
    // "count" holds amount of repeats for each
    // they are parallel vectors
    vector<char> letters; 
    vector<int> count;

    string input = getInput();
    
    loadVectors(count, letters, input);
    deleteAndCount(letters, count);
    sort(count, letters);
    output(count, letters); 
    
    return 0;
}

string getInput()
{
    int period = 0;
    string input;

    do
    {
        cout << "Enter a sentence; Enter a period to end sentence: ";
        getline(cin, input);
        
        for (int i = 0; i < input.size(); i++)
        {
            if (input[i] == '.')
                period += 1;
        }
        
    } while (input.size() == 0 || period == 0);
    
    return input;
}

void loadVectors(vector<int>& count,vector<char>& letters, string& input)
{
    //loads input of string into vector "letters" and stores
    // null values in vector "count
    // stops loading vector when input[i] == '.'
    
    int i = 0;
    
    do
    {
        if (input[i] != ' ')
            letters.push_back(input[i]);
        i++;
    } while (input[i] != '.');
    
    for (int i = 0; i < letters.size(); i++)
    {
        count.push_back(1);
    }
}

void deleteAndCount(vector<char>& letters, vector<int>& count)
{
    // finds repeated letters in vector
    // once found, repeated letters are deleted
    // and counted
    
    for (int i = 0; i < letters.size(); i++)
    {
        for (int j = letters.size(); j > i; j--)
        {
            if (letters[i] == letters[j])
            {
                count[i] += 1;
                swap(letters[j], letters[letters.size() - 1]);
                letters.pop_back();
            }
        }
    }
}

void sort(vector<int>& count, vector<char>& letters)
{
    // using selection sort:
    
    for (int i = 0; i < count.size() - 1; i++)
    {
        for (int j = i + 1; j < count.size(); j++)
        {
            int smallestIndex = i;
            
            if (count[j] > count[smallestIndex])
            {
                swap(count[j], count[smallestIndex]);
                swap(letters[j], letters[smallestIndex]);
            }
        }
    }
}

void output(vector<int>& count, vector<char>& letters)
{
    cout << endl;
    cout << "Character";
    cout << setw(20) << "Number of Repeats" << endl;
    cout << "-----------------------------" << endl;
    
    for (int i = 0; i < letters.size(); i++)
    {
        cout << setw(4) << letters[i];
        cout << setw(17) << count[i] << endl;
    }
}