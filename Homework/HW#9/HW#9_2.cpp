/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/14/14
 * HW: 9
 * Problem: 2
 * I certify this is my own work and code
 */

#include <iostream>
#include <ctype.h>
using namespace std;

string userInput();
bool checkSeat(char seats[][4], int row, int column);
void assignSeat(char seats[][4], int row, int column);
void inputConvert(string seatNum, int &row, int &column);
void seatDefault(char seats [][4]);
void seatOutput(char seats [][4]);
int main()
{
    char user = 'Y';
    string seatNum;
    char seats[7][4];
    seatDefault(seats); // resets seat layout
    
    do
    {
        seatOutput(seats); // outputs display
        seatNum = userInput(); //gets user input
    
        int row;
        int column;
        
        //converts string input into usable int form
    
        inputConvert(seatNum, row, column);
    
        //this checks to see if seat is taken
        
        bool seatEmpty = checkSeat(seats, row, column);
    
        //if seat is taken, keep prompting user to choose a valid seat
        
        while (seatEmpty == false)
        {
            cout << "Seat is taken, try another seat."
            << endl << endl;
        
            seatOutput(seats);
            seatNum = userInput();
        
            inputConvert(seatNum, row, column);
        
            seatEmpty = checkSeat(seats, row, column);
        }
    
        assignSeat(seats, row, column);
        
        cout << endl;
        cout << "Would you like to book another seat?(Y/N):";
        cin >> user;
        cout << endl;
        
    } while (user == 'Y' || user == 'y'); //repeats if user chooses
    
    return 0;
}

//function controls input from user
string userInput()
{
    string seatNum;
    string invalid("qwrtyuiopsfghjklzxvnmQWERTYUIOPSFGHJKLZXVNM890");
    cout << endl;
    
    do
    {
        cout << "Enter a seat to take (Letter, Number): ";
        cin >> seatNum;
        
        //unsure why the check for invalid entrys does not work
        
    } while (seatNum.size() != 2 && invalid.find(seatNum[0]) != -1
             && invalid.find(seatNum[1]) != -1);
    
    seatNum[0] = tolower(seatNum[0]);
    
    return seatNum;
}

//resets seat assignment
void seatDefault(char seats [][4])
{
    for (int i = 0; i < 7; i++)
    {
        seats[i][0] = 'A';
        seats[i][1] = 'B';
        seats[i][2] = 'C';
        seats[i][3] = 'D';
    }
}
//outputs seat layout
void seatOutput(char seats [][4])
{
    for (int i = 0; i < 7; i++)
    {
        int rowNum = i + 1;
        
        cout << rowNum << " ";
        
        for (int j = 0; j < 4; j++)
        {
            cout << seats[i][j] << " ";
        }
        
        cout << endl;
    }
}
//converts string input by user into int values for rows
// and columns 
void inputConvert(string seatNum, int &row, int &column)
{
    switch (seatNum[1])
    {
        case '1':
            row = 0;
            break;
        case '2':
            row = 1;
            break;
        case '3':
            row = 2;
            break;
        case '4':
            row = 3;
            break;
        case '5':
            row = 4;
            break;
        case '6':
            row = 5;
            break;
        case '7':
            row = 6;
            break;
        default:
            row = -1;
            break;
    }
    
    switch (seatNum[0])
    {
        case 'a':
            column = 0;
            break;
        case 'b':
            column = 1;
            break;
        case 'c':
            column = 2;
            break;
        case 'd':
            column = 3;
            break;
        default:
            column = -1;
            break;
    }
}

//if the seat is empty, return true
//if the seat is taken, return false
bool checkSeat(char seats[][4], int row, int column)
{
    if (seats[row][column] != 'X')
        return true;
    else
        return false;
}

//function assigns seat
void assignSeat(char seats[][4], int row, int column)
{
    seats[row][column] = 'X';
    cout << "Your seat has now been assigned." << endl;
    cout << endl;
    
    seatOutput(seats);
}

