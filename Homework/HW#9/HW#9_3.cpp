/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/14/14
 * HW: 9
 * Problem: 3
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

int main()
{
    int a = 42;
    int b = a;
    int* p = &a; //pointer p points to a
    int* q = &b; //pointer q points to b
    
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;
    
    a = 12; //change value of a directly
    
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;
    
    *p = 25; //dereference p
    
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;
    
    *q = *p;//dereference q to p
    *p = 60;//dereference p to 60
    
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;
    
    return 0;
}

