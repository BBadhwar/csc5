/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 3 & 4
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

int* createArray(int size); //returns dyanmic array
void loadArray(int*& a, int& size, int& count);//no return, but changes
// arrays contents
void outputArray(int* a, int count); //used for output
int main()
{
    int size = 5; //intital size of array
    int count = 0; //used for counting values entered by user
    
    int* a = createArray(size); //create dynamic array called 'a'
    loadArray(a, size, count);
    outputArray(a,count);
    
    return 0;
}

int* createArray(int size)
{
    int* array = new int[size];
    
    return array;
    
}

void loadArray(int*& a, int& size, int& count)
{
    cout << "Enter values to add to array." << endl;
    cout << "You can stop adding values by entering '-1': ";
    
    int num;
    
    do
    {
        cin >> num;
        
        // if num == -1, dont add it to array
        // any other number, add to array and add 1
        // to count
        
        if (num != -1) 
        {
            a[count] = num;
            count++;
        }
        
        // when size gets insufficient
        // we need to change size and deallocate memory
        
        if (count >= size)
        {
            size = size * 2;
            
            int* temp = new int[size];
            
            for (int i = 0; i < size; i++)
                temp[i] = a[i];
            
            delete [] a;
            a = temp;
        }
        
    } while (num != -1); // keeps getting values until user enters -1
}

void outputArray(int* a, int count)
{
    
    //outputs all numbers entered by user, excluding -1
    cout << "Array: ";
    for (int i = 0; i < count; i++)
        cout << a[i] << " ";
}

