/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

string* deleteEntry(string* array, int& size, int loc);
int main()
{
    int size = 5;
    int loc = 1; //should delete "Mike"
    
    string* array = new string[size];
    
    array[0] = "Joe";
    array[1] = "Mike";
    array[2] = "Brent";
    array[3] = "Mary";
    array[4] = "Lucy";
    
    //if statement checks for out of bounds loc
    if (loc <= size)
        array = deleteEntry(array, size, loc);
    
    for (int i = 0; i < size; i++)
        cout << array[i] << endl;
    
    return 0;
}

string* deleteEntry(string* array, int& size, int loc)
{
    //size decreases by one for new temp array
    
    size -= 1;
    
    string* temp = new string[size];
    
    //copy up until loc
    
    for (int i = 0; i < loc; i++)
        temp[i] = array[i];
    
    // after loc, copy the values into temp
    // one index lower than array
    // to account for missing space
    
    for (int i = loc + 1; i < size + 1; i++)
        temp[i - 1] = array[i];
    
    delete [] array; //deletes old array
    
    return temp; //returns new array
}

