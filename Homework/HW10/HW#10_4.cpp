/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 6
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

string* addEntry(string* array, int& size, string name);
int main()
{
    int size = 5;
    string name = "Jane"; //new name to be added
    
    string* array = new string[size];
    
    array[0] = "Joe";
    array[1] = "Mike";
    array[2] = "Brent";
    array[3] = "Mary";
    array[4] = "Lucy";
   
    array = addEntry(array, size, name);
    
    for (int i = 0; i < size; i++)
        cout << array[i] << endl;
    
    return 0;
}

string* addEntry(string* array, int& size, string name)
{
    //size increases by one for new temp array
    
    size += 1;
    
    string* temp = new string[size];
    
    
    //for loop copys all of contents of array
    //to a temp array
    
    for (int i = 0; i < size - 1; i++)
        temp[i] = array[i];
    
    
    //new name is added to temp;
    // size - 1 is used to access the last index
    // of the array
    
    temp[size - 1] = name;
    
    delete [] array; //deletes old array
    
    return temp; //returns new array
}
