/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 7
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

string* addEntry(string* array, int& size, string name, int loc);
int main()
{
    int size = 5;
    string name = "Jane"; //new name to be added
    int loc;
    
    cout << "Enter a location for a new name to be added: ";
    
    
    // do while checks for locations above size and
    // those that are below postive 1
    do
    {
        cin >> loc;
        if (loc > size + 1 || loc < 1)
            cout << "Location entered is invalid. Please try again: ";
        
    } while (loc > size + 1 || loc < 1);
    
    string* array = new string[size];
    
    array[0] = "Joe";
    array[1] = "Mike";
    array[2] = "Brent";
    array[3] = "Mary";
    array[4] = "Lucy";
    
    array = addEntry(array, size, name, loc);
    
    for (int i = 0; i < size; i++)
        cout << array[i] << endl;
    
    return 0;
}

string* addEntry(string* array, int& size, string name, int loc)
{
    //size increases by one for new temp array
    
    size += 1;
    
    string* temp = new string[size];
    
    for (int i = 0; i < loc - 1; i++)
        temp[i] = array[i];
    
    temp[loc - 1] = name;
    
    for (int i = loc; i < size; i++)
        temp[i] = array[i - 1];
    
    delete [] array; //deletes old array
    
    return temp; //returns new array
}

