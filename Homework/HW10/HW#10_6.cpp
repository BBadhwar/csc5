/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 8
 * I certify this is my own work and code
 */

#include <iostream>
using namespace std;

int findName(string* array, int size, string name);
string* deleteEntry(string* array, int& size, int loc);
int main()
{
    string name; 
    int size = 5;
    int loc;
    
    //create dynamic array
    
    string* array = new string[size];
    
    //filling array with names
    
    array[0] = "Joe";
    array[1] = "Mike";
    array[2] = "Brent";
    array[3] = "Mary";
    array[4] = "Lucy";
    
    //output of array before
    
    for (int i = 0; i < size; i++)
        cout << array[i] << " ";
    cout << endl;
    
    cout << "Which name would you like to delete: ";
    
    do
    {
        cin >> name;
        loc = findName(array, size, name);
        
        //if name is not found, while loop keeps getting input
        
        if (loc == -1)
            cout << "Name not found; Please try again: ";
        
    } while (loc == -1);
    
    array = deleteEntry(array, size, loc);
    
    //output of array after
    
    for (int i = 0; i < size; i++)
        cout << array[i] << " ";
    cout << endl;
    
    
    return 0;
}

int findName(string* array, int size, string name)
{
    // if name is found, return its index
    // if name does not exist, return -1
    
    for (int i = 0; i < size; i++)
    {
        if (name == array[i])
            return i;
    }
    
    return -1;
    
}

string* deleteEntry(string* array, int& size, int loc)
{
    //size decreases by one for new temp array
    
    size -= 1;
    
    string* temp = new string[size];
    
    //copy up until loc
    
    for (int i = 0; i < loc; i++)
        temp[i] = array[i];
    
    // after loc, copy the values into temp
    // one index lower than array
    // to account for missing space
    
    for (int i = loc + 1; i < size + 1; i++)
        temp[i - 1] = array[i];
    
    delete [] array; //deletes old array
    
    return temp; //returns new array
}

