/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 9
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
using namespace std;


void getData(vector<int>& grades);
void sortVector(vector<int>& grades);
void countArraySize(const vector<int>& grades, int& arraySize);
int* createArray(int arraySize);
void initializeCountArray(int*& countArray, int arraySize);
void fillArray(const vector<int>& grades, int*& array, int*& countArray, int arraySize);
int findSize (const vector<int>& grades);
void output(int* array , int* countArray, int arraySize);


int main()
{
    vector<int> grades;
    int arraySize;
    
    getData(grades);
    sortVector(grades);
    arraySize = findSize(grades);

    int* array;
    int* countArray;
    
    // creating two paralell dynamic arrays:
    // one to hold numbers, the other to hold count
    // intitlizing count array locations to the value of 1
    
    array = createArray(arraySize);
    countArray = createArray(arraySize);
    initializeCountArray(countArray, arraySize);
    fillArray(grades, array, countArray, arraySize);
    output(array , countArray, arraySize);
     
    return 0;
}

void getData(vector<int>& grades)
{
    int num;
    
    cout << "Enter all students grades." << endl;
    cout << "Enter -1 to stop. " << endl;
    
    do
    {
        cout << "Enter: ";
        cin >> num;
        
        if (num != -1)
            grades.push_back(num);
        
    } while (num != -1);
}

void sortVector(vector<int>& grades)
{
    //using selection sort:
    
    for (int i = 0; i < grades.size() - 1; i++)
    {
        for (int j = i + 1; j < grades.size(); j++)
        {
            int smallestIndex = i;
            
            if (grades[j] > grades[smallestIndex])
            {
                swap(grades[j], grades[smallestIndex]);
            }
        }
    }
}

int* createArray(int arraySize)
{
    int* p = new int[arraySize];
    
    return p;
}

void fillArray(const vector<int>& grades, int*& array, int*& countArray, int arraySize)
{
    int loc = 0;
    array[0] = grades[0];
    
    // if grade in vector is equal to next grade:
    // add 1 to count vector
    
    // if grade is not equal to next grade:
    // add a new location with that new grade
    
    for (int i = 0; i < grades.size(); i++)
    {
        if (grades[i] == grades[i + 1])
        {
            countArray[loc]++;
        }
        else if (grades[i] != grades[i + 1])
        {
            loc++;
            array[loc] = grades[i + 1];
        }
    }
}

void initializeCountArray(int*& countArray, int arraySize)
{
    for (int i = 0; i < arraySize; i++)
    {
        countArray[i] = 1;
    }
}

int findSize (const vector<int>& grades)
{
    // counts how many unique grades;
    // gets size for array
    
    int size = 0;
    
    for (int i = 0; i < grades.size(); i++)
    {
        if (grades[i] != grades[i + 1])
        {
            size++;
        }
    }
    
    return size;
}

void output(int* array , int* countArray, int arraySize)
{
    cout << endl;
    
    for (int i = 0; i < arraySize; i++)
    {
        cout << "Number of " << array[i] << "'s: ";
        cout << countArray[i] << endl;
    }
}

