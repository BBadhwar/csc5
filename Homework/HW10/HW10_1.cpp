/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 05/22/14
 * HW: 10
 * Problem: 1 & 2
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
using namespace std;

int* createArray(int size);
void loadArray(int*& a, int size);
void outputArray(int* a, int size);
int main()
{
    srand(time(0));
    
    int size;
    
    cout << "Enter a size for array: ";
    cin >> size;
    
    int* a = createArray(size);
    loadArray(a, size);
    outputArray(a, size);
    
    delete [] a;
    
    return 0;
}

int* createArray(int size)
{
    int* array = new int[size];
    
    return array;
    
}

void loadArray(int*& a, int size)
{
    for (int i = 0; i < size; i++)
        a[i] = rand() % 100;
}

void outputArray(int* a, int size)
{
    for (int i = 0; i < size; i++)
        cout << a[i] << " ";
}

