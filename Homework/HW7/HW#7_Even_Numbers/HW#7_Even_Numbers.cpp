/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 3
 * I certify this is my own work and code
 */

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main()
{
    int num_of_evens = 0;
    vector<int> file_vector;
    ifstream infile;
    string file_name;
    
    cout << "Enter the name of file:";
    cin >> file_name;
    
    infile.open(file_name);
    
    while (!infile.eof())
    {
        int num;
        infile >> num;
        file_vector.push_back(num);
    }
    
    infile.close();
    
    cout << "Data from file: " << endl;
    
    for (int i = 0; i < file_vector.size(); i++)
    {
        cout << file_vector[i] << endl;
    }
    
    for (int i = 0; i < file_vector.size(); i++)
    {
        if (file_vector[i] % 2 == 0)
        {
            num_of_evens++;
        }
    }
    
    cout << endl;
    cout << "Number of even numbers: " << num_of_evens;
    
    return 0;
}




