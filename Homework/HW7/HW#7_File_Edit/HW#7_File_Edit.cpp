/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 5
 * I certify this is my own work and code
 */

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main()
{
    string input_file; //using "original.txt" as input file
    string edit_file;
    string words;
    ifstream infile;
    ofstream edit;
    vector<string> v1;
    
    cout << "Enter name of file you would like to edit spaces: ";
    cin >> input_file;
    cout << "Enter name of file you would like to write to: ";
    cin >> edit_file;
    
    infile.open(input_file); //
    edit.open(edit_file); //
    
    while (infile >> words)
    {
        v1.push_back(words); //stores data in a string vector v1
    }
    
    for (int i = 0; i < v1.size(); i++)
    {
        edit << v1[i] << " "; //outputs each location with a single space between
    }
    
    return 0;
}

