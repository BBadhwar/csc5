/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 1
 * I certify this is my own work and code
 */

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;


int max_value(vector<int> file_vector); // finds max num in vector; returns max value
int min_value(vector<int> file_vector); // finds min num in vector; returns min value
int main()
{
    vector<int> file_vector;
    ifstream infile;
    
    infile.open("data.dat");
    
    while (!infile.eof())
    {
        int num;
        infile >> num;
        file_vector.push_back(num);
    }
    
    infile.close();
    
    cout << "Data from file: " << endl;
    
    for (int i = 0; i < file_vector.size(); i++)
    {
        cout << file_vector[i] << endl;
    }
    
    cout << endl;
    cout << "Max value: " << max_value(file_vector) << endl;
    cout << "Min value: " << min_value(file_vector) << endl;
    
    return 0;
}

int max_value(vector<int> file_vector) // finds max num in vector; returns max value
{
    int previous_num = 0; // previous number in sequence
    int max_num = 0; //max value returned by function
    
    for (int i = 0; i < file_vector.size(); i++)
    {
        if (file_vector[i] > previous_num) // if [i] is greater than previous number
        {
            previous_num = file_vector[i];
            max_num = previous_num;
        }
    }
    
    return max_num;
}

int min_value(vector<int> file_vector) // finds min num in vector; returns min value
{
    int previous_num; // previous number in sequence
    int min_num = 0; //min value returned by function
    
    for (int i = 0; i < file_vector.size(); i++)
    {
        if (file_vector[i] < previous_num) // if [i] is less than than previous number
        {
            previous_num = file_vector[i];
            min_num = previous_num;
        }
    }
    
    return min_num;
}

