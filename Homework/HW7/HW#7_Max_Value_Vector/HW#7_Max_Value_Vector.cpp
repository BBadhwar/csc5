/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 9 & 10
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

void vector_output(vector<int> rand_v) // no return value; used soley for output
{
    for (int i = 0; i < 20; i++)
    {
        cout << rand_v[i] << endl;
    }
    
    cout << endl << endl;
}

int max_value(vector<int> rand_v) // finds max num in vector; returns max value
{
    int previous_num = 0; // previous number in sequence
    int max_num = 0; //max value returned by function
    
    for (int i = 0; i < 20; i++)
    {
        if (rand_v[i] > previous_num) // if [i] is greater than previous number
        {
            previous_num = rand_v[i];
            max_num = previous_num;
        }
    }
    
    return max_num;
}
int main()
{
    vector<int> rand_v; // empty vector
    int max_num = 0;
    
    srand(time(0));
    
    for (int i = 0; i < 20; i++) //for loop fills vector with random numbers
    {
        int num = rand() % 50; // 0 - 50 random nums (for simplicity)
        rand_v.push_back(num);
    }
    
    vector_output(rand_v); // function for output
    
    max_num = max_value(rand_v); // calls max value function
    
    cout << "The maximum value in the vector is: " << max_num;
    
    return 0;
}

