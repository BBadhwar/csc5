/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main()
{
    int median = 0;
    int loc_of_median; //location of median
    ifstream infile;
    vector<int> data_vector;
    
    infile.open("data.dat"); //reads data.dat
    
    while (!infile.eof())
    {
        int num;
        infile >> num;
        data_vector.push_back(num);
    }
    
    for (int i = 0; i < data_vector.size(); i++) //outputs vector
    {
        cout << data_vector[i] << endl;
    }
    
    if (data_vector.size() % 2 != 0) // if sequence contains odd number of elements
    {
        loc_of_median = data_vector.size() / 2;
        median = data_vector[loc_of_median];
    }
    else // if sequence contains even number of elements
    {
        loc_of_median = data_vector.size() / 2;
        median = (data_vector[loc_of_median - 1] + data_vector[loc_of_median]) / 2;
    }
    
    cout << endl;
    cout << "The median is: " << median << endl;
    
    infile.close();

    return 0;
}

