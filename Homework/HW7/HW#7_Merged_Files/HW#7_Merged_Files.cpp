/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 6
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
#include <fstream> 
using namespace std;

// DOES NOT WORK
//
// Cannot find how to sort numbers
//
//

void fileMerge (ifstream& file1, ifstream& file2, ofstream& outfile);
int main()
{
    ifstream file1;
    ifstream file2;
    ofstream outfile;
    
    file1.open("data1.dat");
    file2.open("data2.dat");
    outfile.open("out.dat");
    
    fileMerge(file1, file2, outfile);
    
    file1.close();
    file2.close();
    outfile.close();
    
    return 0;
}
void fileMerge (ifstream& file1, ifstream& file2, ofstream& outfile)
{
    int num1, num2;
    vector<int> unsorted_nums;
    vector<int> sorted_nums;
    
    while (!file1.eof() && !file2.eof())
    {
        file1 >> num1;
        file2 >> num2;
        unsorted_nums.push_back(num1);
        unsorted_nums.push_back(num2);
    }
    
    int previous_num; // previous number in sequence
    int min_num = 0; //min value returned by function
    
    for (int i = 0; i < unsorted_nums.size(); i++)
    {
        if (unsorted_nums[i] < previous_num) // if [i] is less than than previous number
        {
            previous_num = unsorted_nums[i];
            min_num = previous_num;
            sorted_nums.push_back(min_num);
            unsorted_nums.erase(unsorted_nums.begin() + i);
        }
    }
    
    
    for (int i = 0; i < sorted_nums.size(); i++)
    {
        outfile << sorted_nums[i] << " ";
        cout << sorted_nums[i] << endl;
    }
}





