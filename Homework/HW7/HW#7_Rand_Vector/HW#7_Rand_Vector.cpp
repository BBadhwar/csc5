/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/13/14
 * HW: 7
 * Problem: 7 & 8
 * I certify this is my own work and code
 */

#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

int main()
{
    srand(time(0));
    int odd_nums = 0;
    
    vector<int> rand_v; // empty vector
    
    for (int i = 0; i < 10; i++) //for loop fills vector with random numbers
    {
        int num = rand() % 11; // 0 - 10 random nums (for simplicity)
        rand_v.push_back(num);
    }
    
    for (int i = 0; i < 10; i++) // shows vector has been filled
    {
        cout << rand_v[i] << endl;
        
        if (rand_v[i] % 2 != 0) // checks for odd numbers
        {
            odd_nums += 1; // counts odd numbers
        }
    }
    
    cout << endl;
    cout << "Number of odd numbers in vector: " << odd_nums;
    
    return 0;
}

