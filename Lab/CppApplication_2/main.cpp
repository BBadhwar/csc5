/*
 * Name: Brent Badhwar
 * Student ID: 2481409
 * Date: 03/14/14
 * HW: 3
 * Problem: 4
 * I certify this is my own work and code
 */

#include <iostream>

using namespace std;

int main()
{
    int num, positive_sum = 0, negitive_sum = 0,
    sum_all = 0, positive_avg, negitive_avg, total_avg;
    
    for(int i = 1; i <= 10; i++)
    {
        cout << "Enter a number: ";
        cin >> num;
        
                if(num > 0)
                {
                        positive_sum = positive_sum + num;
                }
        
                else
                {
                        negitive_sum = negitive_sum + num;
                }
        
        sum_all = sum_all + num;
    }
    
    cout << endl;
    
    positive_avg = positive_sum / 10;
    negitive_avg = negitive_sum / 10;
    total_avg = sum_all / 10;
    
    cout << "The sum of all positive numbers is: ";
    cout << positive_sum << endl;
    
    cout << "The average of all positive numbers is: ";
    cout << positive_avg << endl;
    
    cout << "The sum of all negitive numbers is: ";
    cout << negitive_sum << endl;
    
    cout << "The average of all negitive numbers is: ";
    cout << negitive_avg << endl;
    
    cout << "The sum of all numbers is: ";
    cout << sum_all << endl;
    
    cout << "The average of all numbers is: ";
    cout << total_avg << endl;
   
    return 0;
}
