/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 11:07 AM
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main() {
    
    string name1, name2, name3; //student names
    int quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7, quiz8,
    quiz9, quiz10, quiz11, quiz12; // total of scores
    double avg1, avg2, avg3, avg4; //averages
    
    cout << "Enter three names of each student: "; //get each quiz
    cin >> name1 >> name2 >> name3;
    cout << endl;
    
    cout << name1 << ", enter four quiz scores: ";
    cin >> quiz1 >> quiz2 >> quiz3 >> quiz4;
    cout << endl;
    
    cout << name2 << ", enter four quiz scores: ";
    cin >> quiz5 >> quiz6 >> quiz7 >> quiz8;
    cout << endl;
    
    cout << name3 << ", enter four quiz scores: ";
    cin >> quiz9 >> quiz10 >> quiz11 >> quiz12;
    cout << endl;
    
    cout << "Name" << setw(10) << "Quiz 1" << setw(10) << "Quiz 2" << setw(10)
    << "Quiz 3" << setw(10) << "Quiz 4" << endl;
    cout << "----" << setw(10) << "------" << setw(10) << "------" << setw(10)
    << "------" << setw(10) << "------" << endl; //start of table
    
    cout << name1 << setw(10) << quiz1 << setw(10) << quiz2 << setw(10)
    << quiz3 << setw(10) << quiz4 << endl;
    
    cout << name2 << setw(10) << quiz5 << setw(10) << quiz6 << setw(10)
    << quiz7 << setw(10) << quiz8 << endl;
    
    cout << name3 << setw(10) << quiz9 << setw(10) << quiz10 << setw(10)
    << quiz11 << setw(10) << quiz12 << endl;
    
    cout << endl;
    
    avg1 = (quiz1 + quiz5 + quiz9) / 3; // defining all of the avgs
    avg2 = (quiz2 + quiz6 + quiz10) / 3;
    avg3 = (quiz3 + quiz7 + quiz11) / 3;
    avg4 = (quiz4 + quiz8 + quiz12) / 3;
    
    cout << setprecision(2) << fixed << "Average" << setw(10) << avg1 << setw(10)
    << avg2 << setw(10) << avg3 << setw(10) << avg4 << endl;
    
   return 0;
}

