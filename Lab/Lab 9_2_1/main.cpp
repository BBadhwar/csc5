/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 24, 2014, 11:47 AM
 */

#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

/*
 * 
 */
int main() 
{
    vector<int> v1;
    
    v1.push_back(3);
    v1.push_back(6);
    v1.push_back(8);
    
    for(int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << endl;
    }
    
    return 0;
}

