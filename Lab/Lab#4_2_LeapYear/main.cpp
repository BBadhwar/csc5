/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 10:46 AM
 */

#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    int year;
    
    do
    {
    
        cout << "Enter a year: ";
        cin >> year;
        
        if(year % 100 == 0 && year > 0)
        {
                cout << year << " is not a leap year." << endl;
        }
        else if(year % 4 == 0 || year % 400 == 0)
        {
                cout << year << " is a leap year" << endl;
        }
        
    }
    while(year > 0);
    return 0;
}

