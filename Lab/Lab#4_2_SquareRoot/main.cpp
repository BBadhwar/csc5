/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 11:29 AM
 */

#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    int n;
    double guess, r;
    
    cout << "Enter a number you would like";
    cout << " to find the square root of: ";
    cin >> n;
    cout << endl;
    
    cout << "Ok, now guess the square root of this number: ";
    cin >> guess;
    cout << endl;
    
    do
    {
    r = n / guess;
    
    guess = (guess + r ) / 2;
    
    cout << "The square root of " << n;
    cout << " is: " << guess << endl;
    
    }
    
    
    
   
    
    return 0;
}
