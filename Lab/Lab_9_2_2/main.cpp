/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 24, 2014, 11:54 AM
 */

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

/*
 * 
 */

int getNumber()
{
    cout << "Enter a number: ";
    int num;
    cin >> num;
    return num;
}
int main() 
{
    vector<int> numbers;
    
    int num;
    
    num = 0;
    
    do
    {
        num = getNumber();
        numbers.push_back(num);
    } while (num != -1);
    
    for (int i = 0; i < numbers.size(); i++)
    {
        cout << numbers[i] << endl;
    }
    
    return 0;
}

