/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 10:33 AM
 */

#include <iostream>
#include <cstdlib>
using namespace std;

/*
 * 
 */
int main() {
    
    int randNum, guess;
    
    srand(time(0));
    
    randNum = rand() % 101;
    
    cout << "Enter a number from 1 - 100: ";
    
    do
    {
        cin >> guess;
       
        if(guess != randNum)
        {
            if(guess > 100 || guess <= 0)
            {
                cout << "Sorry! Your guess must be within 1 - 100" << endl;
                cout << "Guess again: ";
            }
            else if(guess > randNum)
            {
                cout << "Your guess is too high!" << endl;
                cout << "Guess again: ";
            }
            else 
            {
                cout << "Your guess is too low!" << endl;
                cout << "Guess again: ";
            }
       
         }
        else
        {
            cout << "Correct! The magic number is " << randNum << "!";
        }
    } while(guess != randNum);
     
    return 0;
    
    }
        

