/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 12:32 PM
 */
#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    string name;
    
    cout << "Hello! my name is Hal!" << endl << "What is your name: ";
    
    cin >> name;
            
    cout << "Nice to meet you, " << name;
    
    return 0;
}

