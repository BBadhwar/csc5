/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 12:01 PM
 */

#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    int num, new_num;
    
    cout << "Enter a Value: ";
    cin >> num;
    
    new_num = num + 5;
    
    cout << new_num;
    
     return 0;
}

