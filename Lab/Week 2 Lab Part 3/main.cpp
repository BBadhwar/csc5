/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 10:02 AM
 */

#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    char num1, num2, num3, num4, num5; //store each value in unique variable
    
    cout << "Enter a number with five digits: ";
    cin >> num1 >> num2 >> num3 >> num4 >> num5; //get each digit
    
    cout << num1 << " " << num2 << " " << num3 << " " << num4 << " " << num5;
    // outputs space between digits
    
    return 0;
}