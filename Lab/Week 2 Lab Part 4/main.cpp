/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 10:38 AM
 */

#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {
    
    double meters, miles, feet, inches;
    
    cout << "Please enter a value in meters: ";
    cin >> meters; //gets meters from user
    
    miles = meters / 1609; // calculates conversions
    feet = meters * 3.28;
    inches = feet * 12;
    
    cout << meters << " Meters is: " << endl;
           
    
    cout << miles << " Miles" << endl; //outputs conversions
    cout << feet << " Feet" << endl;
    cout << inches << " Inches" << endl;
        

    return 0;
}

