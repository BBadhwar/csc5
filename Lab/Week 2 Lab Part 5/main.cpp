/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 10:38 AM
 */


#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main() {

  double x = 10.76;
  double y = 1.50;
  const int mult = 100;
  
  double dif = x - y;
  cout << dif << endl;
  
  int intDif = static_cast<int>(dif * mult);

  cout << intDif << endl;
  cout << dif * mult << endl;

    
    return 0;
}

